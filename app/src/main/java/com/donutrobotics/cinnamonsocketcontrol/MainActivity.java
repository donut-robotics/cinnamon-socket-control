package com.donutrobotics.cinnamonsocketcontrol;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.donutrobotics.cinnamon_socket.Cinnamon;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Cinnamon cinnamon = ((BaseApplication) getApplication()).getCinnamon();
        setupBtn(findViewById(R.id.btn_left), cinnamon::moveLeft, cinnamon::stopX);
        setupBtn(findViewById(R.id.btn_right), cinnamon::moveRight, cinnamon::stopX);
        setupBtn(findViewById(R.id.btn_up), cinnamon::moveUp, cinnamon::stopY);
        setupBtn(findViewById(R.id.btn_down), cinnamon::moveDown, cinnamon::stopY);

        cinnamon.setUpdateUrl("http://donutrobotics.candypop.jp/update/v2/board/");

        findViewById(R.id.btn_swipe).setOnClickListener(v -> cinnamon.swipe(45, 135));
        findViewById(R.id.btn_home).setOnClickListener(v -> cinnamon.home());
        findViewById(R.id.btn_stop).setOnClickListener(v -> cinnamon.stop());
        findViewById(R.id.btn_check_update).setOnClickListener(v -> cinnamon.checkUpdate(version ->
                runOnUiThread(() -> Toast.makeText(this, "New version " + version, Toast.LENGTH_SHORT).show())));
        findViewById(R.id.btn_update).setOnClickListener(v -> cinnamon.update(null, res ->
                runOnUiThread(() -> Toast.makeText(this, "Updated " + res, Toast.LENGTH_SHORT).show())));

        cinnamon.setListener(new Cinnamon.SocketListener() {
            @Override
            public void onConnected() {
                runOnUiThread(() -> Toast.makeText(MainActivity.this, "Connected", Toast.LENGTH_SHORT).show());

                cinnamon.getIrState(state -> runOnUiThread(() -> Toast.makeText(MainActivity.this, "IR state: " + state, Toast.LENGTH_SHORT).show()));
            }

            @Override
            public void onDisconnected() {
                runOnUiThread(() -> Toast.makeText(MainActivity.this, "Disconnected", Toast.LENGTH_SHORT).show());
            }
        });

        cinnamon.setIrListener(irState -> runOnUiThread(() -> Toast.makeText(this, "IR state: " + irState, Toast.LENGTH_SHORT).show()));
    }

    private void setupBtn(View btn, Runnable onDown, Runnable onUp) {
        btn.setOnTouchListener((view, motionEvent) -> {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    onDown.run();
                    break;
                case MotionEvent.ACTION_UP:
                    onUp.run();
                    view.performClick();
                    break;
            }
            return true;
        });
    }
}