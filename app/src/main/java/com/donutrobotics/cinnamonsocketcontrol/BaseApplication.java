package com.donutrobotics.cinnamonsocketcontrol;

import android.app.Application;

import com.donutrobotics.cinnamon_socket.Cinnamon;

public class BaseApplication extends Application {
    private Cinnamon cinnamon;

    @Override
    public void onCreate() {
        super.onCreate();
        cinnamon = new Cinnamon();
        cinnamon.connect();
    }

    public Cinnamon getCinnamon() {
        return cinnamon;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        cinnamon.disconnect();
    }
}
