package com.donutrobotics.cinnamon_socket;

import android.util.Log;

import androidx.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.Locale;
import java.util.function.Consumer;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;

public class Cinnamon {
    public static final int DEFAULT_PORT = 8080;
    public static final String EVENT_CONNECT = "connect";
    public static final String EVENT_DISCONNECT = "disconnect";

    private static final String TAG = Cinnamon.class.getSimpleName();

    // update result codes
    public static final int UPDATE_SUCCESS = 0;
    public static final int ERROR_UNINITIALIZED = 1;
    public static final int ERROR_UPDATE_LIST_EMPTY = 2;
    public static final int ERROR_DOWNLOAD = 3;
    public static final int ERROR_BOARD_OFFLINE = 4;

    // events
    private static final String STOP_X = "s_x"; // stop base movement
    private static final String STOP_Y = "s_y"; // stop neck movement
    private static final String MOVE = "m"; // general move command, which takes up, down, left, right directions as argument
    private static final String HOME = "h"; // move both base and neck servos to home position
    private static final String STOP = "s"; // stop all movements
    private static final String SWIPE = "sw"; // swipe movement for base motor
    private static final String SWIPE_V = "sv"; // swipe movement for neck motor
    private static final String IR_STATE = "ir"; // get current IR state
    private static final String VERSION = "v"; // get current version
    private static final String UPDATE = "up"; // update firmware

    // dir parameter of move event
    public static final String UP = "u";
    public static final String DOWN = "d";
    public static final String LEFT = "l";
    public static final String RIGHT = "r";

    private Socket mSocket;
    private volatile boolean isConnecting;
    private SocketListener mListener;
    private Consumer<Boolean> mIrListener;
    private Consumer<Boolean> mUpdateListener;

    // updater
    private Updater mUpdater;

    public Cinnamon() {
        this(DEFAULT_PORT);
    }

    public Cinnamon(int port) {
        try {
            // connect to Raspberry Pi control board on localhost, with the help of adb tcp reverse
            mSocket = IO.socket(String.format(Locale.US, "http://localhost:%d/", port));

            // setup event listeners
            mSocket.on(EVENT_CONNECT, args -> {
                Log.d(TAG, EVENT_CONNECT);
                isConnecting = true;
                if (mListener != null) {
                    mListener.onConnected();
                }
            }).on(EVENT_DISCONNECT, args -> {
                isConnecting = false;
                Log.d(TAG, EVENT_DISCONNECT);
                if (mListener != null) {
                    mListener.onDisconnected();
                }
                // also send IR off event when Cinnamon disconnected
                if (mIrListener != null) {
                    mIrListener.accept(false);
                }
            }).on(IR_STATE, args -> {
                boolean state = (boolean) args[0];
                if (mIrListener != null) {
                    mIrListener.accept(state);
                }
            }).on(UPDATE, args -> {
                // firmware update feedback
                boolean res = (boolean) args[0];
                if (mUpdateListener != null) {
                    mUpdateListener.accept(res);
                }
            });
        } catch (URISyntaxException e) {
            // ignored
            Log.e(TAG, e.getMessage());
        }
    }

    public void setListener(SocketListener listener) {
        mListener = listener;
        if (isConnecting) {
            mListener.onConnected();
        }
    }

    public void setUpdateListener(Consumer<Boolean> listener) {
        mUpdateListener = listener;
    }

    public void setIrListener(Consumer<Boolean> listener) {
        mIrListener = listener;
    }

    public void setUpdateUrl(String url) {
        mUpdater = new Updater(url);
    }

    public void connect() {
        // start connecting
        mSocket.connect();
    }

    public void move(String dir) {
        mSocket.emit(MOVE, dir);
    }

    public void moveUp() {
        move(UP);
    }

    public void moveDown() {
        move(DOWN);
    }

    public void moveLeft() {
        move(LEFT);
    }

    public void moveRight() {
        move(RIGHT);
    }

    public void home() {
        mSocket.emit(HOME);
    }

    public void stop() {
        mSocket.emit(STOP);
    }

    public void stopX() {
        mSocket.emit(STOP_X);
    }

    public void stopY() {
        mSocket.emit(STOP_Y);
    }

    public void swipe() {
        mSocket.emit(SWIPE);
    }

    public void swipe(int from, int to) {
        mSocket.emit(SWIPE, from, to);
    }

    public void swipe(int from, int to, int speed) {
        mSocket.emit(SWIPE, from, to, speed);
    }

    public void swipeV() {
        mSocket.emit(SWIPE_V);
    }

    public void swipeV(int from, int to) {
        mSocket.emit(SWIPE_V, from, to);
    }

    public void swipeV(int from, int to, int speed) {
        mSocket.emit(SWIPE_V, from, to, speed);
    }

    public void sendCommand(String command, Object... args) {
        mSocket.emit(command, args);
    }

    public void checkUpdate(Consumer<String> result) {
        if (mUpdater == null) {
            result.accept(null);
            return;
        }

        // get current version
        mSocket.emit(VERSION, (Ack) args -> {
            String current = (String) args[0];

            // check for new version
            mUpdater.getVersionList(versions -> {
                if (!versions.isEmpty() && !current.equals(versions.get(0))) {
                    // new update available
                    result.accept(versions.get(0));
                } else {
                    // no update
                    result.accept(null);
                }
            });
        });
    }

    public void update(@Nullable String version, Consumer<Integer> onResult) {
        if (mUpdater == null) {
            onResult.accept(ERROR_UNINITIALIZED);
            return;
        }

        if (version != null) {
            // update to specified version
            downloadAndUpdate(version, onResult);
        } else {
            // get version list
            mUpdater.getVersionList(versions -> {
                if (!versions.isEmpty()) {
                    // update to latest version
                    downloadAndUpdate(versions.get(0), onResult);
                } else {
                    onResult.accept(ERROR_UPDATE_LIST_EMPTY);
                }
            });
        }
    }

    private void downloadAndUpdate(String version, Consumer<Integer> onResult) {
        // download file
        mUpdater.download(version, bytes -> {
            if (bytes != null) {
                // send to control board
                if (mSocket.connected()) {
                    // split file into small parts, because Socket.io does not allow
                    // emitting large package
                    int bytesLeft = bytes.length;
                    int part = 100 * 1024; // 100KB part
                    int partCount = (int) Math.ceil((float) bytesLeft / part);
                    // unique id for each update request
                    String reqId = "t" + System.currentTimeMillis();
                    try {
                        int partIndex = 0;
                        // send part by part
                        while (bytesLeft > 0) {
                            int tobeCopied = Math.min(bytesLeft, part);
                            byte[] tobeSent = new byte[tobeCopied];
                            System.arraycopy(bytes, bytes.length - bytesLeft, tobeSent, 0, tobeCopied);

                            // send each part with additional data for combining later
                            JSONObject updateMsg = new JSONObject();
                            updateMsg.put("id", reqId);
                            updateMsg.put("count", partCount);
                            updateMsg.put("index", partIndex);
                            updateMsg.put("data", tobeSent);

                            partIndex++;
                            bytesLeft -= tobeCopied;
                            final int bytesLeftFinal = bytesLeft;
                            mSocket.emit(UPDATE, updateMsg, (Ack) args -> {
                                // last part sent
                                if (bytesLeftFinal <= 0) {
                                    onResult.accept(UPDATE_SUCCESS);
                                }
                            });
                        }
                    } catch (JSONException ignored) {
                        // noway JSON exception could be thrown here...
                    }
                } else {
                    onResult.accept(ERROR_BOARD_OFFLINE);
                }
            } else {
                onResult.accept(ERROR_DOWNLOAD);
            }
        });
    }

    public void getIrState(Consumer<Boolean> onResult) {
        mSocket.emit(IR_STATE, (Ack) args -> onResult.accept((boolean) args[0]));
    }

    public void disconnect() {
        mSocket.disconnect();
    }

    public interface SocketListener {
        void onConnected();

        void onDisconnected();
    }
}
