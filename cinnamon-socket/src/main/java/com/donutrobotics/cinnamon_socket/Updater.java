package com.donutrobotics.cinnamon_socket;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Locale;
import java.util.function.Consumer;

public class Updater {
    private static final String VERSION_LIST = "versions.json";
    private static final String EXTENSION = ".tar.gz";

    private final String mBaseUrl;

    public Updater(String baseUrl) {
        mBaseUrl = baseUrl;
    }

    /**
     * This will fetch version list in a sub-folder.
     *
     * @param listener to retrieve result.
     */
    public void getVersionList(@NonNull Consumer<ArrayList<String>> listener) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        DownloadFileAsync task = new DownloadFileAsync(new DownloadFileAsync.Listener() {
            @Override
            public boolean onData(byte[] data, int length) {
                out.write(data, 0, length);
                return true;
            }

            @Override
            public void onFinish(String tag) {
                ArrayList<String> result = new ArrayList<>();
                if (tag != null) {
                    // get string data from byte array
                    String rawData = null;
                    try {
                        rawData = out.toString("UTF-8");
                        out.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    // parse string into json
                    if (rawData != null) {
                        try {
                            JSONObject object = new JSONObject(rawData);
                            // parse version list
                            JSONArray list = object.getJSONArray("versions");
                            for (int i = 0; i < list.length(); i++) {
                                result.add(list.getString(i));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                listener.accept(result);
            }
        });
        // download version list
        String fullUrl = String.format(Locale.US, "%s%s", mBaseUrl, VERSION_LIST);
        task.execute(fullUrl, "version");
    }

    /**
     * Download an update file specified by a string returned by {@link Updater#getVersionList(Consumer)}.
     */
    public void download(String fileName, @Nullable Consumer<byte[]> onFileReceived) {
        if (!fileName.toLowerCase().endsWith(EXTENSION)) {
            fileName += EXTENSION;
        }

        // download update file
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        DownloadFileAsync task = new DownloadFileAsync(new DownloadFileAsync.Listener() {
            @Override
            public boolean onData(byte[] data, int length) {
                out.write(data, 0, length);
                return true;
            }

            @Override
            public void onFinish(String tag) {
                if (tag != null) {
                    if (onFileReceived != null) {
                        byte[] bytes = out.toByteArray();
                        onFileReceived.accept(bytes);
                    }
                } else {
                    if (onFileReceived != null) {
                        onFileReceived.accept(null);
                    }
                }
            }
        });
        // download version list
        String fullUrl = String.format(Locale.US, "%s%s", mBaseUrl, fileName);
        task.execute(fullUrl, "update");
    }
}
