package com.donutrobotics.cinnamon_socket;

import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadFileAsync extends AsyncTask<String, Integer, String> {
    public interface Listener {
        /**
         * For processing chunk of data received
         *
         * @param data   a chunk of data
         * @param length actual data length
         * @return true to continue the download, false to cancel
         */
        boolean onData(byte[] data, int length);

        void onFinish(String fileName);
    }

    private final Listener mListener;

    public DownloadFileAsync(Listener listener) {
        super();
        mListener = listener;
    }

    @Override
    protected String doInBackground(String... strings) {
        String result = null;
        InputStream input = null;

        try {
            URL url = new URL(strings[0]);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            if (strings.length > 3) {
                // additional header fields
                for (int i = 1; i < strings.length / 2; i++) {
                    String key = strings[i * 2];
                    String value = strings[i * 2 + 1];
                    connection.addRequestProperty(key, value);
                }
            }
            connection.connect();

            // for downloading
            input = new BufferedInputStream(url.openStream(), 8192);

            byte[] data = new byte[1024]; // data buffer
            int count;
            boolean cont = true;
            while (cont && (count = input.read(data)) != -1) {
                if (mListener != null) {
                    cont = mListener.onData(data, count);
                    // stop if cont = false
                }
            }
            // downloading process has reached the end successfully
            if (cont) {
                result = strings[1]; // file name
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // closing streams
            try {
                if (input != null) {
                    input.close();
                }
            } catch (IOException ignored) {
            }
        }
        return result;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (mListener != null) {
            mListener.onFinish(s);
        }
    }
}
